// Package import
import React, { Component } from 'react';
import { View, SafeAreaView, RefreshControl, Platform, ScrollView, Text, FlatList, StatusBar } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob'
var RNFS = require('react-native-fs');
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
// Local Import
import Styles from './StatementStyle';
import Icon from 'react-native-vector-icons/Ionicons';
import { Colors } from '../../../utils/Colors';
import PopUpHeader from '../../../component/popupheader';
import RenderItem from '../component/statementRenderItem';
import { CardStatementAction } from '../../../redux/actions/CardActions'
import { Strings } from '../../../utils/Strings';
import { ResponseCode } from '../../../redux/api';
import { heightPercentageToDP } from 'react-native-responsive-screen';
Icon.loadFont()
interface Props {
    navigation?: any;
    route?: any;
    getCardStatement?: any;
    cardStatementResponse?: any
}
const data = [
    {
        "_id": "5f06d97a5aec101a67eb9f89",
        "cardID": "5f06d8c55aec101a67eb9f85",
        "transaction_date": null,
        "cycle_start_date": null,
        "cycle_end_date": null,
        "current_balance": 0
    },
    {
        "_id": "5f06d97a5aec101a67eb9f89",
        "cardID": "5f06d8c55aec101a67eb9f85",
        "transaction_date": null,
        "cycle_start_date": null,
        "cycle_end_date": null,
        "current_balance": 0
    },
    {
        "_id": "5f06d97a5aec101a67eb9f89",
        "cardID": "5f06d8c55aec101a67eb9f85",
        "transaction_date": null,
        "cycle_start_date": null,
        "cycle_end_date": null,
        "current_balance": 0
    }
]
interface state {

    isCheckCard?: any
    refreshing?: boolean
    statementData?: any,
    cardId?: string
}

class Statement extends React.Component<Props, state> {
    constructor(props: Readonly<Props>) {
        super(props);
        this.state = {
            refreshing: false,
            isCheckCard: false,
            statementData: [],
            cardId: this.props.route.params !== undefined && this.props.route.params.cardDetail
        }
    }

    // Class life cycle method
    componentDidMount = () => {
        const { cardId } = this.state
        AsyncStorage.getItem(Strings.Access_Token, (err, token) => {
            if (token !== undefined && token !== null) {
                this.setState({ isCheckCard: true })
                this.props.getCardStatement({
                    access_token: token,
                    cardId: cardId
                })
            }
        });
    }

    componentDidUpdate = (prevProps: any, prevState: any) => {
        if (prevProps.cardStatementResponse !== this.props.cardStatementResponse && prevState.isCheckCard) {
            const { cardStatementResponse } = this.props
            if (cardStatementResponse.code == 401) {
                ResponseCode({ code: 401 })
            }
            else {
                if (cardStatementResponse.code === 200 && cardStatementResponse.data !== null) {
                    this.setState({
                        isCheckCard: false,
                        statementData: cardStatementResponse.data
                    })
                }
                
            }
        }
        
    }

    // Download the statement
    onDownloadImagePress() {
        RNFS.downloadFile({
            fromUrl: "https://via.placeholder.com/300/09f/fff.png%20%20C/O%20https://placeholder.com/",
            toFile: `${RNFS.DocumentDirectoryPath}/statement.pdf`,
        }).promise.then((r: any) => {
            alert("Download Successfully.");
        });
    }
    downloadFile = () => {
        if (Platform.OS === "ios") {
            this.onDownloadImagePress()
        }
        else if (Platform.OS === "android") {
            let randomToken = Math.floor(100000 + Math.random() * 900000);
            const android = RNFetchBlob.android;
            RNFetchBlob.config({
                addAndroidDownloads: {
                    useDownloadManager: true,
                    title: 'statement' + randomToken + '.png',
                    description: 'Tera PDF will be download',
                    mediaScannable: true,
                    notification: true,
                }
            })
                .fetch('GET', "http://www.example.com/file/example.zip")
                .then((res: { path: () => any; }) => {
                    alert("Download Successfully.");
                    android.actionViewIntent(res.path(), 'application/vnd.android.package-archive')
                })
        }
    }

    // Navigate to statement details
    statementDetails = (item: any) => {
        const { navigate } = this.props.navigation
        navigate("statementdetail", item)
    }

    // Navigate to previous screen
    onBackPress = () => {
        this.props.navigation.pop()
    }
    // Pull to refresh
    onRefresh = () => {
        this.setState({ refreshing: false })
    }

    // Main render
    render() {
        const { refreshing, statementData } = this.state
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.White }}>
                <StatusBar barStyle="dark-content" hidden={false}
                    backgroundColor={Colors.Blue_900} />
                <PopUpHeader
                    Ionicons={''}
                    iconName={''}
                    iconVisible={false}
                    openHistory={() => console.warn()}
                    onBackPress={() => this.onBackPress()}
                    name={"Statements"} />
                {statementData.length !== 0 ?
                    <FlatList
                        showsHorizontalScrollIndicator={false}
                        data={statementData}
                        refreshControl={
                            <RefreshControl refreshing={refreshing} onRefresh={this.onRefresh.bind()} />
                        }
                        renderItem={({ item, index }) => (
                            <RenderItem
                                // onClick={() => this.downloadFile()}
                                onClick={() => this.statementDetails(item)}
                                item={item}
                            />
                        )}
                    /> :
                    <View style={{
                        height: heightPercentageToDP(80),
                        justifyContent: 'center', alignItems: 'center'
                    }}>
                        <Text>Statement not found</Text>
                    </View>
                }
            </SafeAreaView >
        );
    }
}

// CONNECT
// ----------------------------------------
const mapStateToProps = (state: any) => ({
    cardStatementResponse: state.CardReducer.cardStatement,

});

// ----------------------------------------
const mapDispatchToProps = (dispatch: any) => {
    return {
        getCardStatement: (payload: any) => dispatch(CardStatementAction(payload)),
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Statement);